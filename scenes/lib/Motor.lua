
local M={}

function M.new(entity)
    local xVelocity,yVelocity,velocityForce,velocityDuration,inboundRemovingMovement
    local Motor={}

    local function movementMotor()
        local xForce,yForce=0,0
        local xCurrentVelocity,yCurrentVelocity=entity:getLinearVelocity()
  
        if 	   xVelocity>0 and xVelocity>xCurrentVelocity then xForce=velocityForce
        elseif xVelocity<0 and xVelocity<xCurrentVelocity then xForce=-velocityForce
        end
        if 	   yVelocity>0 and yVelocity>yCurrentVelocity then yForce=velocityForce
        elseif yVelocity<0 and yVelocity<yCurrentVelocity then yForce=-velocityForce
        end
        entity:applyForce(xForce,yForce,entity.x,entity.y)
    end
  
    function Motor:removeMovementMotor()
        if inboundRemovingMovement~=nil then timer.cancel(inboundRemovingMovement) inboundRemovingMovement=nil end
        if movementMotor~=nil then Runtime:removeEventListener("enterFrame",movementMotor) end
    end

    local function removeM() Motor:removeMovementMotor() end
    local function setM() 
        Runtime:addEventListener("enterFrame",movementMotor) 
        if velocityDuration>=0 then inboundRemovingMovement=timer.performWithDelay(entity.timeMultiplier*velocityDuration,removeM) end
    end
  
    function Motor:applyMovementMotor(velX,velY,velForce,velDuration,delay)
        removeM()
        xVelocity,yVelocity,velocityForce,velocityDuration=velX,velY,velForce,velDuration
        timer.performWithDelay(entity.timeMultiplier*delay,setM)
    end
  
  return Motor
end

return M

