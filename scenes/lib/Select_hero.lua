
local composer = require ("composer")

local scene = composer.newScene()

function scene:create(event)
    local sceneGroup=self.view
    local title_text = display.newText("Choose your hero",display.contentCenterX,display.contentCenterY)
    local button1 = display.newRect(display.contentCenterX+100,display.contentCenterY+100,35,35)
    local button2 = display.newRect(display.contentCenterX-100,display.contentCenterY+100,35,35)
    sceneGroup:insert(title_text)
    sceneGroup:insert(button1)
    sceneGroup:insert(button2)
    function tap1(event)
        if event.phase=="began" then
            composer.gotoScene("scenes.Core", { params={ hero="hero"} } )
        end
    end
    function tap2(event)
        if event.phase=="began" then
            composer.gotoScene("scenes.Core", { params={ hero="hero_test"} } )
        end
    end
    button1:addEventListener("touch",tap1)
    button2:addEventListener("touch",tap2)
end

function scene:destroy( event )
    print("called") --non viene chiamata automaticamente!!!!!!!
    --sceneGroup:removeself()
end

scene:addEventListener("create")
--scene:addEventListener("show")
--scene:addEventListener("hide")
scene:addEventListener("destroy")
return scene