
local M={}

local selectedEntity,selectedAnimation

local currentEntity,animationTested,baseAnimation
local anchorXtest,anchorYtest
local anchorXbase,anchorYbase


local function resetAnchors()
    anchorXtest=0.5
    anchorYtest=0.5
    anchorXbase=nil
    anchorYbase=nil
end

local function setAnchors(entity)
    anchorXtest=0.5
    anchorYtest=0.5
    anchorXbase=entity.anchorX
    anchorYbase=entity.anchorY
end

local function key(event)
    local name = event.keyName
    local phase = event.phase

    if name=="j" and phase=="down" then
        print("-------------")
        print("animation name: "..animationTested)
        print("anchorX -> "..anchorXtest)
        print("anchorY -> "..anchorYtest)
    end

    if name=="k" and phase=="down" then
        currentEntity.anchorX = anchorXtest
        currentEntity.anchorY = anchorYtest
        currentEntity:setSequence(animationTested)
        currentEntity:play()
        print("-------------")
        print("Animation Tested")
    end

    if name=="l" and phase=="down" then
        currentEntity.anchorX = anchorXbase
        currentEntity.anchorY = anchorYbase
        currentEntity:setSequence(baseAnimation)
        currentEntity:play()
        print("-------------")
        print("Base Animation")
    end

    if name=="right" and phase=="down" and anchorXtest<1 and currentEntity.sequence==animationTested then
        anchorXtest=anchorXtest+0.01
        if anchorXtest>1 then anchorXtest=1 end
        currentEntity.anchorX = anchorXtest
    end

    if name=="up"    and phase=="down" and anchorYtest<1 and currentEntity.sequence==animationTested then
        anchorYtest=anchorYtest+0.01
        if anchorYtest>1 then anchorYtest=1 end
        currentEntity.anchorY = anchorYtest
    end

    if name=="left"  and phase=="down" and anchorXtest>0 and currentEntity.sequence==animationTested then
        anchorXtest=anchorXtest-0.01
        if anchorXtest<0 then anchorXtest=0 end
        currentEntity.anchorX = anchorXtest
    end

    if name=="down"  and phase=="down" and anchorYtest>0 and currentEntity.sequence==animationTested then
        anchorYtest=anchorYtest-0.01
        if anchorYtest<0 then anchorYtest=0 end
        currentEntity.anchorY = anchorYtest
    end
end


function M.closeTestMode(e)
    if e==currentEntity then  
        Runtime:removeEventListener("key",key)
        currentEntity.anchorX = anchorXbase
        currentEntity.anchorY = anchorYbase
        currentEntity:setSequence(baseAnimation)
        currentEntity:play() 
        resetAnchors()
        currentEntity.isInTestMode=false
        currentEntity=nil
        animationTested=nil 
        baseAnimation=nil
    end
end

function M.openTestMode(e,a)
    if currentEntity~=nil then M.closeTestMode(currentEntity) end
    currentEntity=e
    animationTested=a
    baseAnimation=currentEntity.sequence
    setAnchors(currentEntity)
    currentEntity.isInTestMode=true
    Runtime:addEventListener("key",key)
end

local function On_Off(event)
    local name = event.keyName
    local phase = event.phase

    if name=="p" then
        if phase=="down" then
            print("-------------")
            print("You entered Test Mode: press the following keys to...")
            print("O: disable it")
            print("L: switch to base model")
            print("K: switch to tested model")
            print("J: see current tested model anchors")
            print("use UP,DOWN,LEFT and RIGHT keys to change anchors of tested model")
            M.openTestMode(selectedEntity,selectedAnimation)
        end
    end

    if name=="o" then
        if phase=="down" and currentEntity~=nil then
            print("-------------")
            print("Test Mode disabled")
            M.closeTestMode(currentEntity)
        end
    end
end

function M.enable_On_Off_keys(e,a)
    selectedEntity=e
    selectedAnimation=a
    print("-------------")
    print("Test mode is avaiable, press P to enable it")
    Runtime:addEventListener("key",On_Off)
end

return M

