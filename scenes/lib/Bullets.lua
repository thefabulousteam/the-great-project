local fx = require( "com.ponywolf.ponyfx" )
local composer = require( "composer" )
local physics = require("physics")
local M={}

--Guardare core e hero_test per avere un'idea di come usare questa classe

local createdBullets=nil
local bulletsOnScreen=0
local maxBulletsOnScreen=250

local function findBulletWithID(ID,bullets)
    if bullets==nil then return nil end
    if bullets[1]==ID  then return bullets[2] end
    return findBulletWithID(ID,bullets[3])
end

local function convertAngle(ang)
    if   ang<0  then return 360+ang end
    if ang>=360 then return ang-360 end
    return ang
end

local function getAngle(x,y)
    local angle=math.deg(math.atan(y/x))
    if x<0 then angle=angle+180 end
    return convertAngle(angle)
end

local function getModularVelocity_Angle(x,y)
    return math.sqrt(x^2+y^2),getAngle(x,y)
end

--per usare un proiettile prima lo creiamo e lo salviamo, indipendentemente se verrà utilizzato o no, ogni proiettile DEVE
--essere creato con un ID unico che servirà dalla funzione .shoot per caricare i suoi dati e "spararlo"
--sheet e sheetdata sono qui valori fissi solo temporaneamente, poiche ho fatto i test con un solo modello di proiettile
--era più facile cosi, in seguito verranno passati come parametri della funzione
function M.creaBullet(ID,image_and_animation,caster,life,initialSpeed,numShoot,homingAngle,shootAngleInterval,delayInterval,bulletShotOnDeath)
    local data={caster,life,initialSpeed,image_and_animation.sprite,image_and_animation.animazione,numShoot,homingAngle,shootAngleInterval,delayInterval,bulletShotOnDeath}
    createdBullets={ID,data,createdBullets}
    return ID
end

--NOTA IMPORTANTE: la funzione .creaBullet ha come valore di ritorno un ID, la funzione .shoot ha come valore di ritorno la classe dei valori
--del proiettile (o proiettili) sparati sullo schermo

--funzione per sparare i proiettili veri e propri, basta inserire l'ID del proiettile per caricare 
--i suoi dati fissi, i dati che invece possono variare, quali l'angolo con cui verrà sparato e la posizione di lancio
--vengono passati come parametri della funzione
function M.shoot(ID,shootX,shootY,shootDelay,shootAngle)
    if ID==nil then return end
    local data=findBulletWithID(ID,createdBullets)
    if data==nil then return end
    local caster=data[1]
    local life=data[2]
    local initialSpeed=data[3] 
    local sheet=data[4]
    local sequenceData=data[5]
    local numShoot=data[6]
    local homingAngle=data[7]/60
    local shootAngleInterval=data[8]
    local delayInterval=data[9]
    local bulletShotOnDeath=data[10]

    local progressiveShootAngle=shootAngle
    local firedBullets=0
    --local acceleration,gravity
    --local isPenetrate,isPenetrateMap
    
    -- qui ho sperimentato una cosa interessante, possiamo trattare i proiettili al momento del lancio come una classe a parte
    --i dati del proiettile fissi sono quelli che rimangono salvati in "created bullets" ma possiamo modificare i dati nel momento del lancio come più ci
    --fa comodo, le modifiche influenzeranno solo quel proiettile (o proiettili nel caso di numShoot superiore a 1) e non influenzeranno i dati fissi
    local projectile={}
    --es. modifichiamo la durata del proiettile
    function projectile:setLife(x) life=x end --potremmo anche gestire i valori come "caster,life, etc" come valori tipo "projectile.life"
    function projectile:getLife() return life end
    function projectile:setShootAngleInterval(x) shootAngleInterval=x end
    function projectile:getShootAngleInterval() return shootAngleInterval end
    
    --questa funzione si occupa della realizzazione del proiettile come entita fisica nel gioco, ogni proiettile è creato qui e anche i colpi multipli
    --sono considerati entità indipendenti fra loro, anche se condividono gli stessi dati
    local function shootBullet()
        if bulletsOnScreen>=maxBulletsOnScreen then return end bulletsOnScreen=bulletsOnScreen+1 
        local bullet=display.newSprite(sheet,sequenceData)
        local modularVelocity,currentAngle
        local isOnScreen=true --questa variabile serve per evitare che un proiettile, dopo che sia stato gia eliminato dallo schermo non cerchi di "cancellarsi" di nuovo a causa del suo timer di morte
        local spdX=(initialSpeed*math.cos(math.rad(progressiveShootAngle)))
        local spdY=(initialSpeed*math.sin(math.rad(progressiveShootAngle)))
        physics.addBody(bullet,"dynamic",{radius=50,isSensor=true})
        bullet.isFixedRotation = true
        bullet.type="bullet"
        bullet.xScale=7
        bullet.yScale=7
        bullet.gravityScale=0
        bullet.team=caster.team
        bullet.x,bullet.y = shootX,shootY
        local targetEntity=Map:getCloserEnemy(bullet)
        bullet:setLinearVelocity(100*spdX,100*spdY)
        bullet:setSequence("Play")
        bullet:play()
        Map:insert(bullet)

        progressiveShootAngle=progressiveShootAngle+shootAngleInterval
        firedBullets=firedBullets+1  
        
        local function destroyBullet()
            if not isOnScreen then return end bulletsOnScreen=bulletsOnScreen-1
            isOnScreen=false                
            currentAngle=getAngle(bullet:getLinearVelocity())    --alla morte il proiettile può sparare un altro proiettile completamente indipendente dal primo
            if bulletShotOnDeath~=nil then M.shoot(bulletShotOnDeath,bullet.x,bullet.y,0,currentAngle) end
            bullet:removeSelf()                                                                         
        end                                                                                             
        timer.performWithDelay(life*1000,destroyBullet)

        if firedBullets<numShoot then
            if delayInterval==0 then shootBullet()
            else timer.performWithDelay(delayInterval,shootBullet) end
        end

        function bullet:collision(event)
            local phase = event.phase
            local other = event.other
            if other~=caster and other.team~=nil and other.type~="bullet" and other.type~="hitbox" then
                isOnScreen=false
                bullet:removeSelf()
                bulletsOnScreen=bulletsOnScreen-1  
            end
            --if other.type=="insect" then
                --isOnScreen=false
                --bullet:removeSelf()
                --bulletsOnScreen=bulletsOnScreen-1  
            --end
            --if other.type=="hero_test" and phase=="began" then
                --print("collided") 
            --end
        end
        bullet:addEventListener("collision")

        local function enterFrame()
            if not isOnScreen or homingAngle==0 then return end

            if targetEntity==nil or targetEntity.isVisible==nil then
               targetEntity=Map:getCloserEnemy(bullet)
               return
            end

            modularVelocity,currentAngle=getModularVelocity_Angle(bullet:getLinearVelocity())

            local deltaX,deltaY=targetEntity.x-bullet.x,targetEntity.y-bullet.y
            local aimedAngle=getAngle(deltaX,deltaY)
            local differentialAngle=aimedAngle-currentAngle
            if differentialAngle==0 then return end

            if math.abs(differentialAngle)>180 then
                if differentialAngle>0 then differentialAngle=differentialAngle-360
                else differentialAngle=360+differentialAngle end
            end

            if math.abs(differentialAngle)>homingAngle then 
                if differentialAngle>0 then differentialAngle=homingAngle
                else differentialAngle=-homingAngle end
            end

            currentAngle=currentAngle+differentialAngle
            spdX=(modularVelocity*math.cos(math.rad(currentAngle)))
            spdY=(modularVelocity*math.sin(math.rad(currentAngle)))
            bullet:setLinearVelocity(0,0)
            bullet:setLinearVelocity(spdX,spdY)
        end
        Runtime:addEventListener("enterFrame",enterFrame )

        function bullet:finalize()    
            bullet:removeEventListener("collision")
            Runtime:removeEventListener("enterFrame",enterFrame )
        end
        bullet:addEventListener("finalize")
    end

    timer.performWithDelay(shootDelay,shootBullet)
    return projectile
end

return M