local fx = require( "com.ponywolf.ponyfx" )
local composer = require( "composer" )
local physics = require("physics")
local M={}

function M.creaHitbox(i,sx,sy,lH,aH,dH,HIV,hd,dt,kb,ku,ta,ka)
    local instance,x,y,width,higth,timespawn,visible,damage,delay
    local Knockback,KnockUp,teamAffiliation,KnockAnimation
    local hitbox
    instance=i    --the caster
    x=sx            --x coordinate for the hitbox relative to the caster
    y=sy            --y coordinate for the hitbox relative to the caster
    width=lH        --hitbox width
    higth=aH        --hitbox heigth
    timespawn=dH    --hitbox duration
    visible=HIV     --is the hitbox visible?
    damage=hd       --hitbox damage
    delay=dt        --delay at which the hitbox will appear on screen
    Knockback=kb    --strength of knockback (default 0)
    KnockUp=ku      --strength of knockup (default 0)
    teamAffiliation=ta             --team of the hitbox (usually the same team as the caster) player team=[0,1] enemy team=6 
    KnockAnimation=ka   --index of the eventual knock animation (0,1,.....) 0=no animation 

    local function kill()
        hitbox:removeSelf()
    end

    local function executeHitboxAfterDelay()
        if instance:isDisabled() then return end
        if not instance then print("instance nil") end
        hitbox = display.newRect(instance.x+x,instance.y+y,width,higth)
        physics.addBody(hitbox)
        Map:insert(hitbox)
        hitbox.isSensor=true
        if visible==nil then hitbox.isVisible=false
            else hitbox.isVisible=visible 
        end
        hitbox.alpha=0.6
        hitbox.name="hitbox"
        hitbox.type="hitbox"
        hitbox.caster=instance
        hitbox.damage=damage
        hitbox.knockback=Knockback
        hitbox.knockup=KnockUp
        hitbox.knockanimation=KnockAnimation
        hitbox.stunAnimationType=nil
        hitbox.gravityScale=0
        hitbox.team=teamAffiliation
        hitbox.description=instance.type.."/"..Knockback.."/"..KnockUp.."/"..KnockAnimation
        Map:insert(hitbox)
        timer.performWithDelay(timespawn,kill)
    end
    timer.performWithDelay(delay,executeHitboxAfterDelay)
end

return M