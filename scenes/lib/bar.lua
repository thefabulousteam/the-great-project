
local M={}

function M.new(x,y,width,hight,maxhp)
  
  local Bar = display.newGroup()
  local background = { 1, 0, 0, 0.75 }
  local foreground = { 0, 1, 0, 0.75 }
  Bar.background = display.newRect(Bar,x,y,width,hight)
  --Bar.background.alpha=0.6
  Bar.foreground = display.newRect(Bar,x,y,width,hight)
  Bar.background:setFillColor(unpack(background))
  Bar.foreground:setFillColor(unpack(foreground))
  Bar.percentlife=1
  
  
  
  function Bar:move(x,y)
    Bar.x=x
    Bar.y=y
  end
  
  function Bar:subtract(hit)
    local percentageloss=hit/maxhp
    Bar.percentlife=Bar.percentlife-percentageloss
    if Bar.percentlife<0.0001 then
      Bar.foreground.isVisible=false
    else
      Bar.foreground.xScale=Bar.percentlife
      Bar.foreground.x=Bar.foreground.x-width*percentageloss/2
    end
    return Bar.percentlife
  end

  function Bar:calculateDinamicLife(currentHp)
    local currentWidth=(width/maxhp)*currentHp
    if currentWidth<=0 then Bar.foreground.isVisible=false  return end
    Bar.foreground.xScale=currentWidth/width
    Bar.foreground.x=x-width/2+currentWidth/2
  end
  
  return Bar
end

return M
