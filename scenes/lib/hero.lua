
-- Module/class for platfomer hero

-- Use this as a template to build an in-game hero 
local fx = require( "com.ponywolf.ponyfx" )
local composer = require( "composer" )
local physics = require("physics")


-- Define module
local M = {}

function M.new( instance, options )
  	--local tapText = display.newText( math.round(instance.x), display.contentCenterX, display.contentCenterY, native.systemFont, 40 )
	-- Get the current scene
  	local hitpoints=20
	local scene = composer.getScene( composer.getSceneName( "current" ) )
	local sounds = scene.sounds
	local teamAffiliation=1
	local shootpause=false

	-- Default options for instance
	options = options or {}

	-- Store map placement and hide placeholder
	instance.isVisible = false
	local parent = instance.parent
	local x, y = instance.x, instance.y
  
	-- Load spritesheet
	local sheetData = { width = 185, height = 160, numFrames = 25, sheetContentWidth = 925, sheetContentHeight = 800}
	local sheet = graphics.newImageSheet( "scenes/img/Hero.png", sheetData )
	local sequenceData = {
	{ name = "idle", frames = {2}},
    { name = "idleshooting", frames = {1}},
	{ name = "walk", frames = {3, 4, 5, 6, 7, 8, 9, 10}, time = 400},
    { name = "walkshooting", frames = { 11, 12, 13, 14, 15, 16, 17, 18}, time = 400},
    { name = "jump", frames = {19}},
    { name = "jumpshooting", frames = {20}},
    { name = "crouch" , frames = {21}},
    { name = "crouchshooting" , frames = {22}}
    }
		
	
	instance = display.newSprite( parent, sheet, sequenceData )
	instance.x,instance.y = x, y
	instance:setSequence( "idle" )


	-- Add camera
	local camera=display.newRect(instance.x,instance.y,10,10)
	camera.type="camera"
	Map:insert(camera)

	-- Add physics
	physics.addBody( instance, "dynamic", { radius = 50, density = 3, bounce = 0, friction =  2.0 } )
	instance.camera=camera
	instance.isFixedRotation = true
   	instance.xScale=1.2
	instance.yScale=1.2
	instance.anchorY = 0.55

	-- Keyboard control
	local max,crouching, acceleration, flip, shooting = 375,false, 0, 1, false
	local sx,dx=false,false


	local function key( event )
		--print(event.keyName)  --Per testare se gli arrivano i comandi
		local phase = event.phase
		local name = event.keyName

		if name=="a" then
			if phase=="down"then
				sx=true
			else
				sx=false
			end
		end

		if name=="d" then
			if phase=="down"then
				dx=true
			else
				dx=false
			end
		end

		if name=="s" then
			if phase=="down" then
				crouching=true
			else
				crouching=false
			end
		end

		if name=="e" then
			if phase=="down" then
				shooting=true
			else
				shooting=false
			end
		end

		if phase=="down" and name=="space" and not instance.jumping  then
			instance:applyLinearImpulse( 0, -550 )
			instance.jumping = true
		end
  end

  local function brigthhero()
    if instance==nil then return true end
    instance.fill.effect="filter.brightness"
    instance.fill.effect.intensity=0
  end
  
  function instance:getHitpoints()
    return hitpoints
  end
  function instance:setHitpoints(new)
    hitpoints= new
  end
	
  function instance:collision( event )
	local phase = event.phase
	local other = event.other
	local y1, y2 = self.y + 50, other.y - ( other.type == "insect" and 25 or other.height/2 )
	local vx, vy = self:getLinearVelocity()
	if other.type=="hitbox" then
		if other.teamOrigin~=teamAffiliation then
			hitpoints=hitpoints-other.damage
			instance.fill.effect="filter.brightness"
			instance.fill.effect.intensity=0.2
			timer.performWithDelay(100,brigthhero)
			instance:applyLinearImpulse(other.knockback,other.knockup)
			if other.knockback~=0 or other.knockup~=0 then
        		instance.isKnockedMidAir=true
           		if other.knockanimation==1 then
           	   --instance:setSequence("KnockUp")
           	   --instance:play()
         		end
            	if other.knockanimation==2 then
            	end
			end
		end
	end

	if phase == "began" then
		if not self.isDead and ( other.type == "blob" or other.type == "insect" ) then
			if y1 < y2 then
					-- Hopped on top of an enemy
				--other:die()
			elseif not other.isDead then
					-- They attacked us
				--self:hurt()
				end
			elseif self.jumping and vy > 0 and not self.isDead then
				-- Landed after jumping
				self.jumping = false
			end
		end
	end

 
  
  local function shoot()
		--print("shoot")
      
		local bullet = display.newImageRect("scenes/img/Bullet.png",20,10)
		physics.addBody(bullet,"dynamic",{radius=1,density=4})
		bullet.gravityScale=0
		bullet.xScale=1
		bullet.yScale=1
		bullet.isBullet=true
		bullet.type="bullet"
		Map:insert(bullet)
		if flip==1 then
		bullet.x=instance.x+100
		bullet:applyLinearImpulse(1,0,bullet.x,bullet.y)
		else
		bullet.x=instance.x-100
		bullet:applyLinearImpulse(-1,0,bullet.x,bullet.y)
		bullet.xScale=-1
		end
		if acceleration~=0 or instance.jumping then
			bullet.y=instance.y - 27
		elseif crouching then
			bullet.y=instance.y
		else
			bullet.y=instance.y - 55
		end
		shootpause=false
  end
  

	local function enterFrame()
		if hitpoints<=0 then
			--print("dead")
		end

		--camera management
		camera.x=instance.x
		camera.y=instance.y-150
		
		--print(acceleration)   --per controllare l'accellerazione in tempo reale
		--print(instance.jumping)
		if acceleration~=0 and not instance.jumping then
			if instance.sequence~="walk" and not shooting then
				instance:setSequence( "walk" )
			end
			if instance.sequence~="walkshooting" and shooting then
				instance:setSequence( "walkshooting" )
			end
			instance:play()
		end

		if crouching and not instance.jumping then
			instance:setSequence("crouch")
			if shooting then
				instance:setSequence( "crouchshooting" )
			end
		end

		if acceleration==0 and not instance.jumping and not crouching then 
			instance:setSequence("idle") 
			if shooting then 
				instance:setSequence("idleshooting") 
			end
		end

		if instance.jumping then
			instance:setSequence("jump")
			if shooting then instance:setSequence("jumpshooting") end
		end

		-- Do this every frame
		local vx, vy = instance:getLinearVelocity()

		--codice per gestire il caso in cui si prema A e D insieme e le relative accellerazioni e flip
		if dx and not sx then acceleration=5000 flip = 1
		elseif sx and not dx then acceleration=-5000 flip = -1
		else acceleration=0 end

		if crouching then acceleration=0
		elseif instance.jumping then acceleration=acceleration/3 end

		if (( acceleration < 0 and vx > -max ) or ( acceleration > 0 and vx < max )) then
			instance:applyForce( acceleration, 0, instance.x, instance.y )
		end
		-- Turn around
		instance.xScale=flip
		--shoot
		if shooting and not shootpause then
			shootpause=true
			timer.performWithDelay(200,shoot)
		end
	end
  
	function instance:finalize()
		-- On remove, cleanup instance, or call directly for non-visual

		instance:removeEventListener( "collision" )
		Runtime:removeEventListener( "enterFrame", enterFrame )
		Runtime:removeEventListener( "key", key )
	end

	-- Add a finalize listener (for display objects only, comment out for non-visual)
	instance:addEventListener( "finalize" )

	-- Add our enterFrame listener
	Runtime:addEventListener( "enterFrame", enterFrame )

	-- Add our key/joystick listeners
	Runtime:addEventListener( "key", key )

	-- Add our collision listeners

	instance:addEventListener( "collision" )

	-- Return instance
	instance.name = "hero"
	instance.type = "hero"
	return instance
end

return M
