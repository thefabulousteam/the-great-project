
local composer = require ("composer")

local scene = composer.newScene()

function scene:create(event)
    local sceneGroup=self.view
    local title_text = display.newText(sceneGroup,"Game Title",display.contentCenterX,display.contentCenterY) -- numbers are coordinates
    local button = display.newRect(sceneGroup,display.contentCenterX,display.contentCenterY+100,35,35)
    local option_menu=display.newGroup()
    option_menu.isVisible=false
    local background_option=display.newRect(option_menu,display.contentCenterX,display.contentCenterY,100,200)
    background_option:setFillColor(unpack({ 1, 0, 0, 0.5 }))
    local button1_option=display.newRect(option_menu,display.contentCenterX,display.contentCenterY-75,35,35)
    local button2_option=display.newRect(option_menu,display.contentCenterX,display.contentCenterY-75,35,35)
    local button_option=display.newRect(sceneGroup,display.contentCenterX-200,display.contentCenterY+200,35,35)
    local time
    local timer_message

    local function gotos()
        composer.gotoScene("scenes.lib.Select_hero", { params={ } } )
    end
   
    local function timer_()
        if timer_message~=nil then timer_message:removeSelf() end

        timer_message = display.newText(sceneGroup,tostring(time),display.contentCenterX,display.contentCenterY+200)

        if time==0 then
            timer_message:removeSelf()
            timer_message = display.newText(sceneGroup,"Loading...",display.contentCenterX,display.contentCenterY+200)
            timer.performWithDelay(1000,gotos)
        else 
            time=time-1
            timer.performWithDelay(300,timer_)
        end
    end
    
    local function Tapped(event)
        if event.phase=="began" then
            button:removeEventListener("touch",Tapped)
            time=3
            timer_()
        end
    end
    button:addEventListener("touch",Tapped)
end

function scene:destroy( event )
    print("called") --non viene chiamata automaticamente!!!!!!!
    --title_text:removeSelf()
    --button:removeSelf()
    --timer_message:removeSelf()
    --collectgarbage()
end

scene:addEventListener("create")
--scene:addEventListener("show")
--scene:addEventListener("hide")
scene:addEventListener("destroy")

return scene