local fx = require( "com.ponywolf.ponyfx" )
local composer= require("composer")
local physics = require("physics")

local M = {}

function M.new(instance,options)
  if not instance then error( "ERROR: Expected display object" ) end
  -------------------- cool
  local teamAffiliation=6
  local maxHitpoints=600
  local hitpoints=600 --400
  local attack=30
  local timeMultiplier=1
  -------------------

  --HealthBar
  local InsectBar = Bar.new(270,30,70,12,maxHitpoints) --x/y/higth/width/hp
  Map:insert(InsectBar)


  local scene = composer.getScene( composer.getSceneName( "current" ) )
  options = options or {}
  instance.isVisible=false

  --valori ereditati dal placeholder
  local parent = instance.parent
  if instance.team~=nil then teamAffiliation=instance.team    end
  local x, y = instance.x, instance.y

  -- Load spritesheet
  local Idle = { width = 591, height = 379, numFrames = 10, sheetContentWidth = 4728, sheetContentHeight = 758 }
  local Strike = { width = 741, height = 416, numFrames = 20, sheetContentWidth = 5928, sheetContentHeight = 1248 }
  local Walk = { width = 648, height = 414, numFrames = 20, sheetContentWidth = 5184, sheetContentHeight = 1242 }
  local Death = { width = 782, height = 381 , numFrames = 30, sheetContentWidth = 7820, sheetContentHeight = 1143 }
  local LookUp = { width = 581, height = 448 , numFrames = 20, sheetContentWidth = 5810, sheetContentHeight = 896 }
  local LookUpIdle = { width = 547, height = 441 , numFrames = 30, sheetContentWidth = 5470, sheetContentHeight = 1323 }
  local KnockUp = { width = 487, height = 605 , numFrames = 30, sheetContentWidth = 4870, sheetContentHeight = 1815 }
  local KnockBack = { width = 569, height = 378 , numFrames = 40, sheetContentWidth = 5690, sheetContentHeight = 1512 }
  local Vertical_strike = { width = 749, height = 629 , numFrames = 20, sheetContentWidth = 2996, sheetContentHeight = 3145 }
  local Leap_preparing = { width = 594, height = 515 , numFrames = 10, sheetContentWidth = 1188, sheetContentHeight = 2575 }
  local Leap_launch = { width = 683, height = 475 , numFrames = 5, sheetContentWidth = 683, sheetContentHeight = 2375 }
  local Leap_midair = { width = 718, height = 342 , numFrames = 5, sheetContentWidth = 718, sheetContentHeight = 1710 }
  local Leap_landing = { width = 866, height = 447 , numFrames = 10, sheetContentWidth = 1732, sheetContentHeight = 2235 }
  
  local Idle_            = graphics.newImageSheet( "scenes/img/enemy/Idle.png", Idle )
  local Strike_          = graphics.newImageSheet( "scenes/img/enemy/Strike.png", Strike )
  local Walk_            = graphics.newImageSheet( "scenes/img/enemy/Walk.png" , Walk )
  local Death_           = graphics.newImageSheet( "scenes/img/enemy/Death.png" , Death )
  local LookUp_          = graphics.newImageSheet( "scenes/img/enemy/Look Up.png" , LookUp )
  local LookUpIdle_      = graphics.newImageSheet( "scenes/img/enemy/Look Up Idle.png" , LookUpIdle )
  local KnockUp_         = graphics.newImageSheet( "scenes/img/enemy/KnockUp.png" , KnockUp )
  local KnockBack_       = graphics.newImageSheet( "scenes/img/enemy/KnockBack.png" , KnockBack )
  local Vertical_strike_ = graphics.newImageSheet( "scenes/img/enemy/Vertical strike.png" , Vertical_strike )
  local Leap_preparing_  = graphics.newImageSheet( "scenes/img/enemy/Leap preparing.png" , Leap_preparing )
  local Leap_launch_     = graphics.newImageSheet( "scenes/img/enemy/Leap launch.png" , Leap_launch )
  local Leap_midair_     = graphics.newImageSheet( "scenes/img/enemy/Leap midair.png" , Leap_midair )
  local Leap_landing_    = graphics.newImageSheet( "scenes/img/enemy/Leap landing.png" , Leap_landing )
  
	local sequenceData = {
		{ name = "Idle", frames = {1,2,3,4,5,6,7,8,9,10},sheet=Idle_,time=400},
    { name = "Attack1", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,20},sheet=Strike_, time=800},
    { name = "Walk" , frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, sheet=Walk_, time=400},
    { name = "Death" , frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30}, sheet=Death_, time=1000},
    { name = "Death_Static" , frames = {30}, sheet=Death_, time=1},
    { name = "LookUp" , frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, sheet=LookUp_, time=275},
    { name = "LookUpIdle" , frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30}, sheet=LookUpIdle_, time=400},
    { name = "KnockUp" , frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30}, sheet=KnockUp_, time=400},
    { name = "KnockBack" , frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,38,40}, sheet=KnockBack_, time=400},
    { name = "Attack2" , frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, sheet=Vertical_strike_, time=900},
    
    { name = "Leap1" , frames = {1,2,3,4,5,6,7,8,9,10}, sheet=Leap_preparing_, time=500},
    { name = "Leap2" , frames = {1,2,3,4,5}, sheet=Leap_launch_, time=150},
    { name = "Leap3" , frames = {1,2,3,4,5}, sheet=Leap_midair_, time=300},
    { name = "Leap4" , frames = {1,2,3,4,5,6,7,8,9,10}, sheet=Leap_landing_, time=350},
    }
		
	
  local instance = display.newSprite( parent, Idle_, sequenceData )
  instance.x,instance.y = x, y
  instance.died=false
  instance:setSequence( "Idle" )
  instance:play()
  instance.xScale=0.5
  instance.yScale=0.5
	
  physics.addBody( instance, "dynamic", { radius=56, density = 3, bounce = 0, friction =  0.3 } )
  --instance.gravityScale=0
	instance.isFixedRotation = true
  instance.anchorY = 0.5 --0.5
  instance.team=teamAffiliation
  instance.timeMultiplier=timeMultiplier

  local flip
  local function updateFlip()
    if instance.xScale<0 then flip=1 
    else flip=-1 end
  end

  local function brigthenm()
    if instance.fill==nil then return end
    instance.fill.effect="filter.brightness"
    instance.fill.effect.intensity=0
  end

  local function getEnemy()
    return Map:getCloserEnemy(instance)
  end

  ---------
  local isDead=false
  local isDying=false
  local isKnockedMidAir=false
  local isStunned=false

  function instance:isDisabled() return isDead or isDying or isKnockedMidAir or isStunned end

  local executingAction=false
  local isInLeap3=false
  local enemy
  local actionTimer=nil
  local walking,walkingSx,walkingDx=false,false,false
  ---------

  -- motori usati dall'entità
  local walkingMotor=motor.new(instance)
  
  local function updateWalking() walking=walkingDx or walkingSx end
  local function stopWalking()
    walkingSx,walkingDx=false,false
    walkingMotor:removeMovementMotor()
    updateWalking()
  end

  local function movetoEnemy(distancex)
    if instance:isDisabled() or executingAction then stopWalking() return end
    if distancex<0 and (not walkingDx) then 
      walkingMotor:applyMovementMotor(300,0,4000,-1,0)
      walkingDx=true
      walkingSx=false
    elseif distancex>0 and (not walkingSx) then 
      walkingMotor:applyMovementMotor(-300,0,4000,-1,0)
      walkingSx=true
      walkingDx=false
    end
    updateWalking()
    if instance.sequence~="Walk" then
      instance.anchorY=0.5
      instance:setSequence("Walk")
      instance:play()
    end
  end

  local function turningAction(distancex)
    if instance:isDisabled() or executingAction then return end
    if distancex<0 then instance.xScale=-0.5
    elseif distancex>0 then instance.xScale=0.5
    end
  end

  local function resetActionTimer()
    if actionTimer==nil then return end
    timer.cancel(actionTimer) 
    actionTimer=nil
  end
  
  local function setActionTimer(time,fun)
    resetActionTimer()
    actionTimer=timer.performWithDelay(time,fun)
  end

  local function cancellaAnimazioni()
    isInLeap3=false
    executingAction=false
    stopWalking()
    resetActionTimer()
    if instance:isDisabled() or instance.sequence=="Idle" then return end
    instance.anchorY=0.5
    instance:setSequence("Idle")
    instance:play()
  end

  local function setKnockBackAnimation()
    cancellaAnimazioni()
    instance:setLinearVelocity(0,0)
    instance.anchorY=0.5
    instance:setSequence("KnockBack")
    instance:play()
  end
  local function setKnockUpAnimation()
    cancellaAnimazioni()
    instance:setLinearVelocity(0,0)
    instance.anchorY=0.5
    instance:setSequence("KnockUp")
    instance:play()
  end

  -- Attacks
  local function attack1()
    if instance:isDisabled() or executingAction then return end executingAction=true
    instance.anchorY=0.5
    instance:setSequence("Attack1")
    instance:play()
    updateFlip()            --100   /try -flip*300,-500/    teamAffiliation
    hitbox.creaHitbox(instance,flip*120,0,100,50,50,false,attack,620,0,0,teamAffiliation,31231)
    --timer.performWithDelay(800,cancellaAnimazioni)
    setActionTimer(800,cancellaAnimazioni)
  end

  local function attack2()
    if instance:isDisabled() or executingAction then return end executingAction=true
    instance.anchorY=0.7
    instance:setSequence("Attack2")
    instance:play()
    updateFlip()           --100   /try -flip*300,-500/    teamAffiliation
    hitbox.creaHitbox(instance,flip*20,-100,250,60,70,false,attack,650,flip*500,-300,teamAffiliation,2)
    --timer.performWithDelay(900,cancellaAnimazioni)
    setActionTimer(900,cancellaAnimazioni)
  end

  local function leap_3()
    if instance:isDisabled() or (not isInLeap3) then return end isInLeap3=false
    instance.anchorY=0.55
    instance:setSequence("Leap4")
    instance:play()
    updateFlip()                                      --70 timespawn
    hitbox.creaHitbox(instance,flip*100,-30,200,120,70,false,attack,150,flip*350,-200,teamAffiliation,2)
    --timer.performWithDelay(350,cancellaAnimazioni)
    setActionTimer(350,cancellaAnimazioni)
  end

  local function attack3()
    if instance:isDisabled() or executingAction then return end executingAction=true
    updateFlip()

    local function leap_jump()
      if instance:isDisabled() then return end executingAction=true
      instance:applyLinearImpulse(flip*700,-500) 
    end -- flip*700,-500

    local function leap_2()
      if instance:isDisabled() then return end executingAction=true
      isInLeap3=true
      instance.anchorY=0.55
      instance:setSequence("Leap3")
      instance:play()
    end

    local function leap_1()
      if instance:isDisabled() then return end executingAction=true
      instance.anchorY=0.6
      instance:setSequence("Leap2")
      instance:play()
      timer.performWithDelay(70,leap_jump)
      --timer.performWithDelay(150,leap_2)
      setActionTimer(150,leap_2)
    end
   
    instance.anchorY=0.7
    instance:setSequence("Leap1")
    instance:play()
    --timer.performWithDelay(500,leap_1)
    setActionTimer(500,leap_1)
  end
  
  local function lookUpIdle()
    if instance:isDisabled() or executingAction or walking then return end
    if instance.sequence~="LookUpIdle" then
      instance.anchorY=0.625
      instance:setSequence("LookUpIdle")
      instance:play()
    end
  end

  local function lookUp()
    if instance:isDisabled() or executingAction or walking then return end
    if instance.sequence~="LookUpIdle" and instance.sequence~="LookUp" then
      instance.anchorY=0.625
      instance:setSequence("LookUp")
      instance:play()
    end
    timer.performWithDelay(275,lookUpIdle)
    --setActionTimer(275,lookUpIdle)
  end
 
  local function AI()
    if instance==nil or instance:isDisabled() or executingAction then stopWalking() return end
    if not pause then
      enemy=getEnemy()
      if enemy==nil then cancellaAnimazioni() return end

      local distX,distY=instance.x-enemy.x,instance.y-enemy.y
      local absDistX,absDistY = math.abs(distX),math.abs(distY)

      turningAction(distX)

      if absDistX<=1000 and absDistX>200 then movetoEnemy(distX)
      else stopWalking() end

      if absDistX<=100 and distY<=150 and distY>0 then attack2()
      elseif absDistX<=200 and absDistY<=50 then attack1()
      elseif absDistX<=200 and distY<=600 and distY>0 then lookUp()
      elseif absDistX<=700 and absDistX>400 and absDistY<=100 then attack3()
      elseif not walking then cancellaAnimazioni() end
    end
  end

  local function death()
    isDead=true
    instance:removeSelf()
    InsectBar:removeSelf()
  end

  local function deathAnimation()
    instance.anchorY=0.4
    instance:setSequence("Death_Static")
    instance:play()
    timer.performWithDelay(1500,death)
  end

  local function die()
    resetActionTimer()
    isDying=true
    instance.died=true
    instance.anchorY=0.4
    instance:setSequence("Death")
    instance:play()
    timer.performWithDelay(1000,deathAnimation)
  end

  local function enterFrame()
    InsectBar:calculateDinamicLife(hitpoints)
    InsectBar:move(instance.x-250,instance.y-130)
    if hitpoints<=0 and not isDying then die() end
    AI()
  end

  function instance:collision(event)
    if not isDying then
      local other = event.other
      local phase = event.phase
      local y1, y2 = self.y + 50, other.y - ( other.type == "insect" and 25 or other.height/2 )
      local vx, vy = self:getLinearVelocity()
      if other.type=="hitbox" and phase == "began" then
        if other.team~=teamAffiliation then
          hitpoints=hitpoints-other.damage
          if other.knockback~=0 or other.knockup~=0 then
            isKnockedMidAir=true
            if other.knockanimation==1 then
              setKnockUpAnimation()
            end
            if other.knockanimation==2 then
              if other.knockback>0 then instance.xScale=0.5 else instance.xScale=-0.5 end
              setKnockBackAnimation()
            end
          end
          instance:applyLinearImpulse(other.knockback,other.knockup)
          instance.fill.effect="filter.brightness"
          instance.fill.effect.intensity=0.2
          timer.performWithDelay(100,brigthenm)
        end
      end
      if other.type~="hitbox" and phase == "began" then
        if not self.isDead and false then       --not self.isDead and ( other.type == "blob" or other.type == "hero" )
          if y1 < y2 then
            -- Hopped on top of an enemy
            --other:die()
          elseif not other.isDead then
             -- They attacked us
            --self:hurt()
          end
        --elseif (vy>0 or other==enemy ) and not isDead then
        elseif isInLeap3 and (other==enemy or vy>0) and not isDead then isKnockedMidAir=false leap_3() 
        elseif isKnockedMidAir and vy>0 and not isDead then isKnockedMidAir=false cancellaAnimazioni()
        end
      end
        ------------

      if other.type=="bullet" then
        --display.remove(other)
        hitpoints=hitpoints-30
        --if hero.x<instance.x then instance:applyLinearImpulse(800,0,instance.x,instance.y) end
        --if hero.x>instance.x then instance:applyLinearImpulse(-800,0,instance.x,instance.y) end
        instance.fill.effect="filter.brightness"
        instance.fill.effect.intensity=0.2
        timer.performWithDelay(100,brigthenm)
      end
      if other.type=="hero_test" then
        --hero_test:setHitpoints(hero_test:getHitpoints()-attack)
      end
    end
  end

  function instance:finalize()
    instance:removeEventListener("collision")
    Runtime:removeEventListener("enterFrame",enterFrame)
  end
  instance:addEventListener("collision")
  instance:addEventListener("finalize")
  Runtime:addEventListener("enterFrame",enterFrame)

  instance.name = "insect"
	instance.type = "insect"
  return instance
end
    
return M