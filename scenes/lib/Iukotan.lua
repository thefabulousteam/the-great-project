local composer= require("composer")
local physics = require("physics")
local hitbox = require("scenes.lib.hitbox")
local M = {}

function M.new(instance)
  if not instance then error( "ERROR: Expected display object" ) end
  -------------------- cool
  local teamAffiliation=6
  local hitpoints=1000
  local attack=20
  local isDead=false
  local hitting=false
  --local flip=0.5
  -------------------
  local scene = composer.getScene(composer.getSceneName( "current" ) )
  instance.isVisible=false
  options = options or {}
  local parent = instance.parent
  local x, y = instance.x, instance.y
  
  local sheetData1 = { width = 646, height = 846, numFrames = 20, sheetContentWidth = 6460, sheetContentHeight = 1692 } --Idle

	local sheet1 = graphics.newImageSheet( "scenes/img/Iukotan/Idle.png", sheetData1 )

  
	local sequenceData = {
		{ name = "Idle", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},sheet=sheet1,time=1200}
    }
		
	
	instance = display.newSprite( parent, sheet1, sequenceData )
	instance.x,instance.y = x, y
  instance.xScale=0.5  --0.5
  instance.yScale=0.5  --0.5
	instance:setSequence( "Idle" )
  instance:play()
  
  physics.addBody( instance, "dynamic", { radius=55, density = 3, bounce = 0, friction =  0.5 } )
	instance.isFixedRotation = true
  instance.anchorY = 0.8
  instance.team=teamAffiliation

  
  local function brigthenm()
    if instance.fill==nil then return true end
    instance.fill.effect="filter.brightness"
    instance.fill.effect.intensity=0
  end
  local function kill()
    HB:removeSelf()
  end
  local function movetohero(distancex)
    local vx, vy = instance:getLinearVelocity()
    if distancex<-50 and vx<300 then
    instance.xScale=-0.5
    instance:applyForce(4000,0,instance.x,instance.y)
    end
    if distancex>50 and vx>-300 then
    instance.xScale=0.5
    instance:applyForce(-4000,0,instance.x,instance.y)
    end
  end

  local function attack1()
    local flip=0
    if instance.xScale<0 then 
    flip=1 else flip=-1 end
    hitbox.new(instance,flip*120,0,100,50,500,true,attack) --riferimento/+x/+y/width/higth/timeframe/visible/damage
    --if hero.x<instance.x then hero:applyLinearImpulse(-300,-300,hero.x,hero.y) end
    --if hero.x>instance.x then hero:applyLinearImpulse(300,-300,hero.x,hero.y) end
    hitting=false
  end

  local function AI()
  end

  function everyFrame()
    AI()
  end

  local function death()
    isDead=true
  end


  local function deathAnimation()
    instance:setSequence("Death_Static")
    instance:play()
  end
  
  function instance:collision(event)
    if isDead then
      self.isVisible=false
      display.remove(self)                         
      return true 
    end
--codice provvisorio per la morte
    if hitpoints<=0 then
      timer.performWithDelay(1,death)
    else
      local other = event.other
      local phase = event.phase
      if other.type=="bullet" then
        --display.remove(other)
        hitpoints=hitpoints-30
        --if hero.x<instance.x then instance:applyLinearImpulse(800,0,instance.x,instance.y) end
        --if hero.x>instance.x then instance:applyLinearImpulse(-800,0,instance.x,instance.y) end
        instance.fill.effect="filter.brightness"
        instance.fill.effect.intensity=0.2
        timer.performWithDelay(100,brigthenm)
      end
      if other.type=="hitbox" then
        if other.teamOrigin~=teamAffiliation then
          hitpoints=hitpoints-other.damage
          instance:applyLinearImpulse(other.knockback,other.knockup)
          if other.knockback~=0 or other.knockup~=0 then
            isKnockedMidAir=true
            if other.knockanimation==1 then
              instance:setSequence("KnockUp")
              instance:play()
            end
            if other.knockanimation==2 then
              instance:setSequence("KnockBack")
              instance:play()
            end
          end
          instance.fill.effect="filter.brightness"
          instance.fill.effect.intensity=0.2
          timer.performWithDelay(100,brigthenm)
        end
      end
    end

    
  end
  
  function instance:finalize()
    instance:removeEventListener("collision")
    instance:removeEventListener("enterFrame",everyFrame)
  end
  instance:addEventListener("collision")
  instance:addEventListener("finalize")
  Runtime:addEventListener("enterFrame",everyFrame)

  instance.name = "Iukotan"
	instance.type = "Iukotan"
  return instance
  
end
    
return M