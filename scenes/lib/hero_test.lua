-- Module/class for platfomer hero

-- Use this as a template to build an in-game hero 
local fx = require( "com.ponywolf.ponyfx" )
local composer = require( "composer" )
local physics = require("physics")
-- Define module
local M = {}

function M.new( instance, options )
  	--local tapText = display.newText( math.round(instance.x), display.contentCenterX, display.contentCenterY, native.systemFont, 40 )
	-- Get the current scene
	local maxHitpoints=500
	local hitpoints=500
	local attack=50  
	local scene = composer.getScene( composer.getSceneName( "current" ) )
	local sounds = scene.sounds
	local teamAffiliation=1
	local shootpause=false
	local timeMultiplier=1
	local healPerSecond=10 --5
	local healPerFrame=healPerSecond/60

	--HealthBar
	local Herobar = Bar.new(270,30,500,20,maxHitpoints) --x/y/higth/width/hp

	-- Default options for instance
	options = options or {}

	-- Store map placement and hide placeholder
	instance.isVisible = false
	local parent = instance.parent
	local x, y = instance.x, instance.y
  
	-- Load spritesheet
	local sheetData1 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Idle
	local sheetData2 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Walk
	local sheetData3 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Attack1windUp
	local sheetData4 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Attack1HitboxSwing
	local sheetData5 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Attack1EndAttackTransition
	local sheetData6 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Attack1SecondAttackFollowUp
	local sheetData7 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Attack2_Unfinished
	--local sheetData8 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Attack3_Unfinished
	--local sheetData9 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Attack4_Unfinished
	local sheetData10 = { width = 1225, height = 1021, numFrames = 20, sheetContentWidth = 6125, sheetContentHeight = 4084 } --Death


	local sheet1 = graphics.newImageSheet( "scenes/img/HeroTest/HeroTest_Idle.png", sheetData1 )
	local sheet2 = graphics.newImageSheet( "scenes/img/HeroTest/HeroTest_Walk.png", sheetData2 )
	local sheet3 = graphics.newImageSheet( "scenes/img/HeroTest/HeroTest_Attack1WindUp.png", sheetData3 )
	local sheet4 = graphics.newImageSheet( "scenes/img/HeroTest/HeroTest_Attack1HitboxSwing.png", sheetData4 )
	local sheet5 = graphics.newImageSheet( "scenes/img/HeroTest/HeroTest_Attack1EndAttackTransition.png", sheetData5 )
	local sheet6 = graphics.newImageSheet( "scenes/img/HeroTest/HeroTest_Attack1SecondAttackFollowUp.png", sheetData6 )
	local sheet7 = graphics.newImageSheet( "scenes/img/HeroTest/Attack2_Unfinished.png", sheetData7 )
	--local sheet8 = graphics.newImageSheet( "scenes/img/HeroTest/Attack3_Unfinished.png", sheetData8 )
	--local sheet9 = graphics.newImageSheet( "scenes/img/HeroTest/Attack4_Unfinished.png", sheetData9 )
	local sheet10 = graphics.newImageSheet( "scenes/img/HeroTest/HeroTest_Death.png", sheetData10 )

	
	local sequenceData = {
		{ name = "Idle", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},						   	sheet=sheet1, time=timeMultiplier*2000},
		{ name = "Walk", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},						   	sheet=sheet2, time=timeMultiplier*2000},
		{ name = "Attack1WindUp", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},			  	sheet=sheet3, time=timeMultiplier*500},
		{ name = "Attack1HitboxSwing", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},		   	sheet=sheet4, time=timeMultiplier*500},
		{ name = "Attack1EndAttackTransition", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, 	sheet=sheet5, time=timeMultiplier*500},
		{ name = "Attack1SecondAttackFollowUp", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, 	sheet=sheet6, time=timeMultiplier*500},
		{ name = "Attack2", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},						sheet=sheet7, time=timeMultiplier*750},
		{ name = "Attack2_Static", frames = {20},																sheet=sheet7, time=timeMultiplier*1},
		{ name = "Attack3", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},						sheet=sheet8, time=timeMultiplier*750},
		{ name = "Attack3_Static", frames = {20},																sheet=sheet8, time=timeMultiplier*750},
		{ name = "Attack4", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},						sheet=sheet9, time=timeMultiplier*750},
		{ name = "Death", frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},						sheet=sheet10, time=timeMultiplier*1250},
		{ name = "Death_Static", frames = {20},																	sheet=sheet10, time=timeMultiplier*1}
	}

	local instance = display.newSprite( parent, sheet1, sequenceData )
	instance.x,instance.y = x, y
	instance.died=false
	instance:setSequence( "Idle" )
	instance:play()

	--bullets loaded in by this entity
	local sheetData = { width = 192, height = 191, numFrames = 10, sheetContentWidth = 1920, sheetContentHeight = 191}
    local sheet = graphics.newImageSheet( "scenes/img/Bullets/rotating bullet.png", sheetData ) --bullets
    local sequenceData = {
        { name = "Play", frames = {1,2,3,4,5,6,7,8,9,10},time=700}
		}
	dati_animazioni={sprite=sheet,animazione=sequenceData}

	local bullet1=bullet.creaBullet(1000,dati_animazioni,instance,10,1,1,0,0,0)
	local bullet2=bullet.creaBullet(1001,dati_animazioni,instance,10,2,1,0,0,0) --ID,image_and_animation,caster,life,initialSpeed,numShoot,homingAngle,shootAngleInterval,delayInterval,bulletShotOnDeath
	
	local Hail_of_homing_bullets_third_bullets=bullet.creaBullet(1002,dati_animazioni,instance,10,20,1,0,0,0)
	local Hail_of_homing_bullets_second_bullets=bullet.creaBullet(1003,dati_animazioni,instance,1,20,1,720,0,0,1002)
	local Hail_of_homing_bullets_first_bullets=bullet.creaBullet(1004,dati_animazioni,instance,4,3,10,0,0,0,1003)

	local sheetData = { width = 123, height = 45, numFrames = 2, sheetContentWidth = 264, sheetContentHeight = 45}
    local sheet = graphics.newImageSheet( "scenes/img/Bullets/LaserBeam.png", sheetData ) --bullets
    local sequenceData = {
        { name = "Play", frames = {1,2},time=150}--time=300
		}
	dati_animazioni={sprite=sheet,animazione=sequenceData}

	local laser_test=bullet.creaBullet(1100,dati_animazioni,instance,30,25,50,0,0,300)--300

	-- Add camera
	local camera=display.newRect(instance.x,instance.y,10,10)
	camera.type="camera"
	Map:insert(camera)

	-- Add physics				
	physics.addBody( instance, "dynamic", { radius = 50, density = 3, bounce = 0, friction =  0.3 })
	instance.camera=camera
	instance.team=teamAffiliation
	instance.isFixedRotation = true
   	instance.xScale=1
	instance.yScale=1
	instance.anchorX = 0.5
	instance.anchorY = 0.53
	instance.timeMultiplier=timeMultiplier
	instance.isInTestMode=false
	
	-- Keyboard control
	local flip=1
	local attacking=false
	local ignoreAmount=0
	local attackingFirstCombo=false
	local attackingSecondCombo1,attackingSecondCombo2=false,false
	local actionOverride=false
	local walking=false

	-- da aggiornare!!!!	
	function instance:isDisabled() return false end

	--Motors used by the entity
	local walkingMotor=motor.new(instance)
	local attackMotor=motor.new(instance)

	local function removeAllMotors()
		walkingMotor:removeMovementMotor()
		attackMotor:removeMovementMotor()
	end

	local function removeWalkingParam()
		walking=false
		walkingMotor:removeMovementMotor()
	end

	-- function used to ensure the model is facing the right direction
	local function updateXscale() instance.xScale=-flip	end

	local function setIdleAnimation()
		if attacking or walking or instance.sequence=="Idle" or instance.died then return end
		instance:setSequence("Idle")
		instance:play()
	end 

	local function setWalkingAnimation()
		if attacking or (not walking) or instance.sequence=="Walk" or instance.died then return end
		instance:setSequence("Walk")
		instance:play()
	end 

	local function resetAnimation()
		attackingFirstCombo=false
		attackingSecondCombo1=false
		attackingSecondCombo2=false
		instance:setSequence("Idle")
		instance:play()
		if not actionOverride then 
			attacking=false 
		else
			actionOverride=false
		end
	  end

	local function resetAttackCombo()
		if ignoreAmount>0 then ignoreAmount=ignoreAmount-1 --print("ignored/"..ignoreAmount) 
		return true end
		--print("notIgnored ")
		if actionOverride then
			--print("resetted an animation")
			actionOverride=false
			resetAnimation()
		end
	  end

	local function enableAnimationOverride()
		actionOverride=true
	end



--Attack 1 combo----------------------------------------------------------------------------------------------
	attackingFirstCombo=false

	local function Attack1SecondAttackFollowUp()
		instance:setSequence( "Attack1SecondAttackFollowUp" )
		instance:play()             
		--transition.moveBy(instance,{x=flip*80, time=timeMultiplier*225,delay=timeMultiplier*225}) --100   /try -flip*300,-500/    teamAffiliation
		attackMotor:applyMovementMotor(flip*356,0,5000,225,225)
		hitbox.creaHitbox(instance,flip*100,-85,600,75,timeMultiplier*50,false,attack,timeMultiplier*250,flip*1000,-300,teamAffiliation,2)
		timer.performWithDelay(timeMultiplier*475,resetAnimation)
	end


	  local function Attack1EndAttackTransition()
		instance:setSequence( "Attack1EndAttackTransition" )
		instance:play()		 			--(500-100)<475 the timer of resetAttackCombo of the current function must be shorter then the timer for the reset animation
		timer.performWithDelay(timeMultiplier*100,enableAnimationOverride)--called in the subsequential attack however this can be prevented incrementing  the ignoreCombo value
		timer.performWithDelay(timeMultiplier*500,resetAttackCombo)		  --EDIT!! it dosen't matter if the current resetAttackCombo happens since the "overrideAnimation" value will be setted to false (cause the attack is an endCombo move)
	  end																  


	  local function Attack1HitboxSwing()
		instance:setSequence( "Attack1HitboxSwing" )
		instance:play()

		local facingAngle

		if flip==1 then facingAngle=0
		elseif flip==-1 then facingAngle=180 end

		local shootAngle=0 --angle on shooting (from positive x axis),initial speed
		
		--local bulletTest=bullet.shoot(bullet1,instance.x+(flip*200),instance.y-50,150,(facingAngle+(flip*shootAngle)))--ID,shootX,shootY,shootDelay,shootAngle
		--bullet.shoot(bullet2,instance.x+(flip*200),instance.y-50,150,(facingAngle+(flip*shootAngle)))
		--bullet.shoot(bullet3,instance.x+(flip*200),instance.y-50,150,(facingAngle+(flip*shootAngle)))
		--bullet.shoot(bullet4,instance.x+(flip*200),instance.y-50,150,(facingAngle+(flip*shootAngle)))
		
		--local bulletN=bullet.shoot(Hail_of_homing_bullets_first_bullets,instance.x+(flip*200),instance.y-50,150,(facingAngle+(flip*shootAngle)))
		--bulletN:setShootAngleInterval(flip*-10)

		--local bulletN=bullet.shoot(laser_test,instance.x+(flip*200),instance.y-50,150,(facingAngle+(flip*shootAngle)))

		--bulletN:setLife(100)
		--bulletTest:setLife(100)

		--transition.moveBy(instance,{x=flip*80, time=timeMultiplier*270})	--100   /try -flip*300,-500/     -600			teamAffiliation
		attackMotor:applyMovementMotor(flip*296,0,5000,270,0)
		hitbox.creaHitbox(instance,flip*200,-120,275,500,timeMultiplier*50,false,attack,timeMultiplier*250,0,-600,teamAffiliation,1)
		timer.performWithDelay(timeMultiplier*270,Attack1EndAttackTransition)	
	  end
	
	  local function Attack1()
		instance:setSequence( "Attack1WindUp" )
		instance:play()
		--transition.moveBy(instance,{x=-flip*40, time=timeMultiplier*350})
		attackMotor:applyMovementMotor(-flip*114,0,5000,350,0)
		timer.performWithDelay(timeMultiplier*350,Attack1HitboxSwing)	
	  end
------------------------------------------------------------------------------------------

---Attack 2/3/4 -----------------------------------------------------------------------------
attackingSecondCombo1=false
attackingSecondCombo2=false


local function Attack4()
	instance:setSequence( "Attack4" )
	instance:play()
	--transition.moveBy(instance,{x=flip*40, time=timeMultiplier*150,delay=timeMultiplier*300})
	attackMotor:applyMovementMotor(flip*266,0,5000,150,300)
	hitbox.creaHitbox(instance,flip*200,-120,275,500,timeMultiplier*50,true,attack,timeMultiplier*250,flip*100,-300,teamAffiliation,0)
	timer.performWithDelay(timeMultiplier*700,resetAnimation)	
  end

local function Attack3_Transition()
	instance:setSequence( "Attack3_Static" )
	instance:play()
	timer.performWithDelay(timeMultiplier*10,enableAnimationOverride)
	timer.performWithDelay(timeMultiplier*300,resetAttackCombo)

end

local function Attack3()
	instance:setSequence( "Attack3" )
	instance:play()
	--transition.moveBy(instance,{x=flip*40, time=timeMultiplier*150})
	attackMotor:applyMovementMotor(flip*266,0,5000,150,0)
	hitbox.creaHitbox(instance,flip*200,-120,275,500,timeMultiplier*50,true,attack,timeMultiplier*250,flip*100,-300,teamAffiliation,0)
	timer.performWithDelay(timeMultiplier*375,Attack3_Transition)	
  end

local function Attack2_Transition()		--those "attacks" define the transition animations for the continuity of a combo
	instance:setSequence( "Attack2_Static" )	--also they enable all the value required to specify that the attack which called them
	instance:play()								--was part of an attack chain
	timer.performWithDelay(timeMultiplier*10,enableAnimationOverride)
	timer.performWithDelay(timeMultiplier*300,resetAttackCombo)

end

local function Attack2()
	instance:setSequence( "Attack2" )
	instance:play()
	--instance:applyLinearImpulse( flip*100, 0 )
	--transition.moveBy(instance,{x=flip*100, time=timeMultiplier*300})
	attackMotor:applyMovementMotor(flip*333,0,5000,300,0)
	hitbox.creaHitbox(instance,flip*200,-120,275,500,timeMultiplier*50,true,attack,timeMultiplier*250,flip*100,-300,teamAffiliation,0)
	timer.performWithDelay(timeMultiplier*500,Attack2_Transition)	
  end


---------------------------------------------------------------------------------------------



	-- Test Mode
	TestMode.enable_On_Off_keys(instance,"Attack1WindUp","Attack2")

  	local A_key,D_key

	local function key( event )
		local name = event.keyName
		local phase = event.phase
		--print(name)    --Per testare se gli arrivano i comandi

		if instance.isInTestMode then return end

		-----disabled test mode 
		if name=="a" then
			if phase=="down" and not attacking then
				A_key=true
				flip=-1
				walking=true
				setWalkingAnimation()
				walkingMotor:applyMovementMotor(-175,0,5000,-1,0)
			else
				A_key=false
				if not D_key then
					removeWalkingParam()
				 end
			end
		end

		if name=="d"then
			if phase=="down" and not attacking then
				D_key=true
				flip=1
				walking=true
				setWalkingAnimation()
				walkingMotor:applyMovementMotor(175,0,5000,-1,0)
			else
				D_key=false
				if not A_key then
					removeWalkingParam()
				end
			end
		end

		if name=="s" then
			if phase=="down" then
				crouching=true
			else
				crouching=false
			end
		end

		if name=="f" then
			if phase=="down" and attackingSecondCombo2 and actionOverride then
				--print("executed F third move")
				resetAnimation()--this must be called everytime to remove precedent animations and combo values
				--ignoreAmount=ignoreAmount+1    --<---this is not necessary here since attack4 dosen't reactivate "actionOverride" cause it's an end combo attack
				Attack4()						 --it must increment the "ignore" if it is an attack that will create a timing window for another attack
			elseif phase=="down" and attackingSecondCombo1 and actionOverride then
				--print("executed F second move")
				resetAnimation()
				ignoreAmount=ignoreAmount+1 --every attack that can be inserted in a combo will recall an animationReset after a timer, if the timer expires before the combo
				--hasn't been updated with new inputs it will reset all the combos, however if more unput is given this value will prevent
				--that the animationReset called by the timer created from the previous attack cancels the current animation
				--the ignore amount sets how many times the inbound animationReset calls will be ignored, so that they won't interfer
				--with the continuity of the animations (basically all the attacks which are in the middle of the "elseif" chain requires this code)
						
				attackingSecondCombo2=true			--you must specify which attack can be activated after another one (which must activate an animation overrid)
				attackingFirstCombo=true			--those attacks have a specific "combo name" definition wired in the specific of the code here
				Attack3()
			elseif phase=="down" and not attacking then
				removeWalkingParam()
				--print("executed F first move")
				attacking=true				--the base attack cannot be used in a combo (more code needed for that)
				attackingSecondCombo1=true
				Attack2()
			end
		end

		if name=="e"  then
			if phase=="down" and attackingFirstCombo and actionOverride then
				--print("executed E second move")
				resetAnimation()
				Attack1SecondAttackFollowUp()
			elseif phase=="down" and not attacking then
				--print("executed E first move")
				removeWalkingParam()
				attacking=true
				attackingFirstCombo=true
				attackingSecondCombo2=true
				Attack1()
			end
		end


		if phase=="down" and name=="space" and not instance.jumping and not attacking then
			instance:applyLinearImpulse( 0, -850 ) -- -550
			instance.jumping = true
		end
  end


  

  



  local function brigthhero()
    if instance==nil then return true end
    instance.fill.effect="filter.brightness"
    instance.fill.effect.intensity=0
  end
  
  function instance:getHitpoints()
    return hitpoints
  end
  function instance:setHitpoints(new)
    hitpoints = new
  end
	
  function instance:collision( event )
	if instance.isInTestMode then return end
	local phase = event.phase
	local other = event.other
	local y1, y2 = self.y + 50, other.y - ( other.type == "enemy" and 25 or other.height/2 )
	local vx, vy = self:getLinearVelocity()
	if other.type=="hitbox" and phase == "began" then
		if other.team~=teamAffiliation then
			hitpoints=hitpoints-other.damage
			instance.fill.effect="filter.brightness"
			instance.fill.effect.intensity=0.2
			timer.performWithDelay(100,brigthhero)
			instance:applyLinearImpulse(other.knockback,other.knockup)
			if other.knockback~=0 or other.knockup~=0 then
        		instance.isKnockedMidAir=true
           		if other.knockanimation==1 then
           	   --instance:setSequence("KnockUp")
           	   --instance:play()
         		end
            	if other.knockanimation==2 then
            	end
			end
		end
	end
	if other.type~="hitbox" and phase == "began" then
		if not self.isDead and ( other.type == "blob" or other.type == "enemy" ) then
			if y1 < y2 then
					-- Hopped on top of an enemy
				other:die()
			elseif not other.isDead then
					-- They attacked us
				self:hurt()
				end
			elseif self.jumping and vy > 0 and not self.isDead then
				-- Landed after jumping
				self.jumping = false
			end
		end
	end

	local function death()
		instance:removeSelf()
		Herobar:removeSelf()
	end

	local function deathAnimation()
	 	instance:setSequence("Death_Static")
		instance:play()
		timer.performWithDelay(1500,death)
	end

	local function heal()
		if hitpoints<=0 then return end
		hitpoints=hitpoints+healPerFrame
		if hitpoints>maxHitpoints then hitpoints=maxHitpoints end
	end

	local function enterFrame()
		if instance.isInTestMode then return end
		--reset Idle under certain circumstances
		setIdleAnimation()

		-- Turn around
		updateXscale()

		--optional heal
		heal()
		
		--healthbar management
		Herobar:calculateDinamicLife(hitpoints)

		--camera management
		camera.x=instance.x
		camera.y=instance.y-150

		if hitpoints<=0 and not instance.died then
			instance.died=true
			attacking=true
			removeAllMotors()
			instance:setSequence( "Death" )
			instance:play()
			timer.performWithDelay(timeMultiplier*1250,deathAnimation)
			instance:finalize()
		end

		--jump
		if instance.jumping then
			--instance:setSequence("jump")
			if shooting then 
				--instance:setSequence("jumpshooting") 
			end
		end

		--shoot
		if shooting and not shootpause then
			shootpause=true
			--timer.performWithDelay(200,shoot)
		end
	end
  
	function instance:finalize()
		-- On remove, cleanup instance, or call directly for non-visual
		instance:removeEventListener( "collision" )
		Runtime:removeEventListener( "enterFrame", enterFrame )
		Runtime:removeEventListener( "key", key )
	end

	-- Add a finalize listener (for display objects only, comment out for non-visual)
	instance:addEventListener( "finalize" )

	-- Add our enterFrame listener
	Runtime:addEventListener( "enterFrame", enterFrame )

	-- Add our key/joystick listeners
	Runtime:addEventListener( "key", key )

	-- Add our collision listeners
	instance:addEventListener( "collision" )

	-- Return instance
	instance.name = "hero_test"
	instance.type = "hero_test"
	return instance
end

return M