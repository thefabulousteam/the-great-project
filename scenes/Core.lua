
-- Include modules/libraries
local composer = require "composer"
local tiled = require( "com.ponywolf.ponytiled" )
local json = require( "json" )
local physics = require("physics")

--Global modules
bullet = require("scenes.lib.Bullets")
healthBars = require("scenes.lib.bar")
hitbox = require("scenes.lib.hitbox")
Bar = require("scenes.lib.bar")
motor = require("scenes.lib.Motor")
TestMode = require("scenes.lib.TestMode")

-- Variables local to scene
local soggetto


-- Create a new Composer scene
local scene = composer.newScene()

-- This function is called when scene is created
function scene:create( event )
	local sceneGroup = self.view  -- Add scene display objects to this group
  physics.start()
	physics.setGravity( 0, 32 )
  local MapData = json.decodeFile( system.pathForFile( "Mymap/test2/Livello1.json", system.ResourceDirectory ) )
	Map = tiled.new( MapData, "Mymap/test2" )
	Map.x, Map.y = display.contentCenterX - Map.designedWidth/2, display.contentCenterY - Map.designedHeight/2
	sceneGroup:insert( Map )
	print(event.params.hero)
	selected_hero=event.params.hero
	--Map.xScale=0.4
	--Map.yScale=0.4
	Map.xScale=0.6
	Map.yScale=0.6
  
	Map.extensions="scenes.lib."
	
	Map:extend("hero","hero_test","insect","Iukotan")
	Map:loadSpawn_point(selected_hero,"Start_point_1")


	player1 = Map:findObject( selected_hero )
	--hero=Map:findObject( "hero" )
  --hero_test=Map:findObject( "hero_test" )
	--insect= Map:findObject("insect")
	--Iukotan= Map:findObject("Iukotan")
  parallax = Map:findLayer( "parallax" )
	soggetto=player1.camera
end

local function enterFrame(event)
	---scorro personaggio
  if soggetto and soggetto.x and soggetto.y and not soggetto.isDead then
		local x, y = soggetto:localToContent( 0, 0 )
		x, y = display.contentCenterX - x, display.contentCenterY - y
		Map.x, Map.y = Map.x + x, Map.y + y
		if parallax then
			parallax.x, parallax.y = Map.x / 6, Map.y / 8
		end
	end
end

-- This function is called when scene comes fully on screen
function scene:show( event )
	local phase = event.phase
	if ( phase == "will" ) then
		Runtime:addEventListener( "enterFrame", enterFrame )
	elseif ( phase == "did" ) then
	end
end

-- This function is called when scene goes fully off screen
function scene:hide( event )
	local phase = event.phase
	if ( phase == "will" ) then
	elseif ( phase == "did" ) then
		Runtime:removeEventListener( "enterFrame", enterFrame )
	end
end

-- This function is called when scene is destroyed
function scene:destroy( event )
  --collectgarbage()
end

scene:addEventListener("create")
scene:addEventListener("show")
scene:addEventListener("hide")
scene:addEventListener("destroy")

return scene
